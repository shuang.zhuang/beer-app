import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BeerService {

  constructor(private httpClient: HttpClient) { }

  public getBeer(): Observable<Beer[]> {
    return this.httpClient.get<Beer[]>('https://api.punkapi.com/v2/beers');
  }
}

export interface Beer {
  name: string;
  description: string;
  image_url: string;
}
