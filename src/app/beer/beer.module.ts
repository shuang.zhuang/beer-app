import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BeerRoutingModule } from './beer-routing.module';
import { BeerComponent } from '../beer/beer.component';


@NgModule({
  declarations: [
    BeerComponent,
  ],
  imports: [
    CommonModule,
    BeerRoutingModule
  ]
})
export class BeerModule { }
