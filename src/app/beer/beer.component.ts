import { Component, OnInit } from '@angular/core';
import {Beer, BeerService} from "../services/beer.service";

@Component({
  selector: 'app-beer',
  templateUrl: './beer.component.html',
  styleUrls: ['./beer.component.css']
})
export class BeerComponent implements OnInit {

  public beers: Beer[] = [];
  public date = new Date();
  constructor(private beerService: BeerService) { }

  ngOnInit(): void {
    this.beerService.getBeer().subscribe((beers: Beer[]) => {
      this.beers = beers;
      console.log(this.beers);
    })
  }

}
